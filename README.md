# tvon/dotfiles

An uncurated repo for my dopfile crap.

## New Install Notes

### Ubuntu

#### CapsLock -> Control

Depends on the device/hardware. For a ThinkPad t460p:

Create `/etc/udev/hwdb.d/90-caps-ctrl.hwdb` containing the following:

```
evdev:atkbd:dmi:bvn*:bvr*:bd*:svnLENOVO:pn*:pvr*
  KEYBOARD_KEY_3a=leftctrl
```

Could probably be improved to be more generic.

#### HIDPI

Tiny Grub UI:

In grub prompt run `videoinfo`, pick a reasonable resolutoin (e.g. half of the native resolution) and set in `/etc/default/grub` as `GRUB_GFXMODE`.

Tiny Console:

`sudo dpkg-reconfigure console-setup` and pick the largest font (probably).

Spotify:

Customize `.desktop` file to add `--force-device-scale-factor=1`.  Note that when installed via Snap the `.desktop` file is located in `/var/lib/snapd/desktop/applications` (and is named `spotify_spotify.desktop`).

### Budgie

## Notes

Zim does not setup the prompt if in the console.  I'm sure there are good reasons for this but in my case I'd like to have it:

```
autoload -Uz promptinit && promptinit
prompt liquidprompt
```

