# dotfiles

dotfiles = Rake::FileList.new("*") do |files|
  files.exclude("README.md")
  files.exclude("Rakefile")
  files.exclude("config")
  files.exclude("local")
end

# Special treatment, we don't want to symlink ~/.config or ~/.local into the
# git repository because all sorts of apps write there.
subdirectories = Rake::FileList.new("config/*", "local/*")

allfiles = dotfiles + subdirectories
allfiles.each do |dotfile|
  desc "#{dotfile.pathmap('~/.%p')}"
  file dotfile.pathmap('~/.%p') => dotfile do |task|
    source = File.expand_path(task.source)
    target = File.expand_path(task.name)

    # Skip files that are already there or identical
    if File.exist?(target) && File.identical?(source, target)
      puts "Skipping existing file #{target} == #{target}"
      next
    end

    # Directories are symlinks so this should work
    if File.exist?(target)
      puts "Backing up existing file #{target} => #{target}.dotfiles-backup"
      File.rename(target,"#{target}.dotfiles-backup")
    end

    puts "Linking #{source} => #{target}"
    File.symlink(source, target)
  end
end

desc "Link all files to ~/"
task default: allfiles.pathmap('~/.%p')
