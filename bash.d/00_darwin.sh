#!/usr/bin/env bash

OS=${OSTYPE//[0-9.]/}
if [ "$OS" != "darwin" ]; then
  return 1
fi

alias ls="ls -CFG"

if [ -f /opt/homebrew/bin/brew ]; then
  export PATH=/opt/homebrew/bin:$PATH
  export MANPATH=/opt/homebrew/share/man:$MANPATH
fi

export PGDATA=~/.local/var/postgres

if [ -d ~/.local/bin ]; then
  export PATH=$PATH:~/.local/bin
fi
