#!/usr/bin/env bash

if command -v keychain > /dev/null 2>&1
then
  KEYS=$(basename -s .pub ~/.ssh/*.pub)
  eval `keychain --eval --quiet --agents ssh --inherit any ${KEYS}`
fi
