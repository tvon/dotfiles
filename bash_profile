#!/usr/bin/env bash

if [ -f ~/.bashrc ]; then
  source ~/.bashrc
fi

export GOPATH=${HOME}/go
export PATH=${HOME}/.local/bin:${PATH}
export PATH="$HOME/.cargo/bin:$PATH"
export PATH=$GOPATH/bin:$PATH
export PATH=/home/tvon/.npm-packages/bin:$PATH

export MANPATH=$MANPATH:/opt/homebrew/opt/erlang/lib/erlang/man
