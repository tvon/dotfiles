#!/usr/bin/env bash

if [ -d ~/.bash.d ]; then
  for file in `ls -1 ~/.bash.d/*.sh | sort -n`; do
    #echo "debug: sourcing $file"
    source $file
  done
fi

alias be="bundle exec"
alias brake="bundle exec rake"
alias brails="bundle exec rails"
alias update-brew="brew update && brew outdated"

man() {
  # GROFF_NO_SGR=1 requried on nix for some reason
  env LESS_TERMCAP_mb=$'\E[01;31m' \
    LESS_TERMCAP_md=$'\E[01;38;5;74m' \
    LESS_TERMCAP_me=$'\E[0m' \
    LESS_TERMCAP_se=$'\E[0m' \
    LESS_TERMCAP_so=$'\E[38;5;246m' \
    LESS_TERMCAP_ue=$'\E[0m' \
    LESS_TERMCAP_us=$'\E[04;38;5;146m' \
    man "$@"
}

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

if [ -f "${HOME}/.liquidprompt/liquidprompt" ]; then
  [[ $- = *i* ]] && source "$HOME/.liquidprompt/liquidprompt"
fi

if [ -f ~/.bashrc-local ]; then
  source ~/.bashrc-local
fi
