Disable "hold for special characters" option

```
defaults write -g ApplePressAndHoldEnabled -bool false
```


HTML

The old html-5-boilerplate "hot pink" selection background color:

```
#fe57a1
```

A blue alternative someone suggested that ain't bad (but it's not as eye popping as the pink):

```
#3297FD
```

Their current default:

```
#b3d4fc
```

RFC-3339 date format

```console
date '+%FT%T.%N%:z'
```

# When macbook audio goes to hell

```console
sudo launchctl stop com.apple.audio.coreaudiod && sudo launchctl start com.apple.audio.coreaudiod
```

# Fetch mac address for device form CLI

```console
$ cat /sys/class/net/eth0/address
```

# Misc

Dealing with a full `/tmp` because passenger is erroring and there are so many files that glob completion won't work.

```console
# Path may vary but, chdir to avoid bad `rm -rf` usage:
$ cd /tmp/systemd-private-*-httpd24-httpd.service-*/tmp
# Use find to get around there being so many files:
$ find . -name 'passenger*' -exec rm -f {} \;
```

Do look at the errors first, please.

# Working with `tar`

```console
$ docker save alpine -o alpine.tar
$ # list tarball contents
$ tar -tf alpine.tar
39cb81dcd06e3d4e2b813f56b72da567696fa9a59b652bd477615b31af969239/
39cb81dcd06e3d4e2b813f56b72da567696fa9a59b652bd477615b31af969239/VERSION
39cb81dcd06e3d4e2b813f56b72da567696fa9a59b652bd477615b31af969239/json
39cb81dcd06e3d4e2b813f56b72da567696fa9a59b652bd477615b31af969239/layer.tar
e7d92cdc71feacf90708cb59182d0df1b911f8ae022d29e8e95d75ca6a99776a.json
manifest.json
repositories
$ # extract a single file from tarball
$ tar -xf alpine.tar manifest.json
$ ls manifest.json
manifest.json
$ # dump contents of a single file to stdout
$ tar -xOf alpine.tar manifest.json | jq
[
  {
    "Config": "e7d92cdc71feacf90708cb59182d0df1b911f8ae022d29e8e95d75ca6a99776a.json",
    "RepoTags": [
      "alpine:latest"
    ],
    "Layers": [
      "39cb81dcd06e3d4e2b813f56b72da567696fa9a59b652bd477615b31af969239/layer.tar"
    ]
  }
]
```

Since I seem to have to look this up every now and then, the permissions of `~/.ssh`:

```
~/.ssh            700 (drwx------)
~/.ssh/id_rsa.pub 644 (-rw-r--r--)
~/.ssh/id_rsa     600 (-rw-------)
```

I always forget the exact syntax of shell io redirection:

```
$ some-cmd 2>&1
```

Push an empty git commit (e.g., to trigger CI)

```console
$ git commit --allow-empty -m "Empty commit"
$ git push
```

Find/replace in a git repo, one way:

```console
$ perl -pi -e 's/oldstring/newstring/g' `git grep --name-only oldstring`
```

Show invisible characters:

```console
$ cat -vte
$ # e.g.:
$ echo "$IFS" | cat -vte
 ^I$
^@$
```

Use this for sanity:

```
#!/usr/bin/env bash
set -eu -o pipefail
```

Explained:

- `-o pipefail` - exit if any command in a pipe fails (normally it is only the last command that matters)
- `-e` - `errexit` - exit script on any failure
- `-u` - `nounset` - fail on using variables that are not defined in the script
  (taken from https://sipb.mit.edu/doc/safe-shell/ I think)

[Full docs found here](https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html#The-Set-Builtin)

## Tmux

Send command to all panes:

`^J` (or whatever the prefix is):

```
:setw synchronize-panes on
```

Toggle with binding in `tmux.conf`:

```
bind C-S set-window-option synchronize-panes\; display-message "synchronize-panes is now #{?pane_synchronized,on,off}"
```

### Homebrew things

Dealing with special brew location:

```console
$ pip install --global-option=build_ext \
  --global-option="-I/opt/homebrew/include/" \
  --global-option="-L/opt/homebrew/lib" ...
```

or

```console
$ pip install --global-option=build_ext \
  --global-option="-I$(brew --prefix)/include/" \
  --global-option="-L$(brew --prefix)/lib" ...
```

## Unsorted Random Bits

I'll reformat this all at some point.

# Messy but print all groups you're in

```console
$ id | python -c 'from __future__ import print_function; import sys; data = sys.stdin.read(); groups = data.split("groups=")[-1]; [print(group) for group in groups.split(",")]' | sort
```

## ldapsearch

ldapsearch -o ldif-wrap=no -LLL samaccountname=$USER

## macOS

Log interface (kinda like journalctl)

log stream

## Ansible

Gather facts

ansible all -i some-inventory-file -m setup

Can add `--tree $PWD/facts`

## Shell Scripts

## YUM

The RHEL equivalent of "build-essential"

$ yum groupinstall "Development Tools"

List groups/metapackages in repo:

$ repoquery -ga --repoid=REPO

List packages in group:

$ repoquery -gl GROUP

List packages in repo:

$ repoquery -qa --repoid=REPO

Get data on all groups (e.g., to find out what groups containe a package):

$ yum groupinfo '\*' | less

## Docker

Get a label value:

$ docker inspect --format '{{ index .Config.Labels "com.covermymeds.ci.build_url"}}' <image-name>

Delete exited containers:

$ docker rm $(docker ps -f status=exited -q)

> The status filter matches containers by status. You can filter using created, restarting, running, removing, paused, exited and dead. For example, to filter for running containers:

Delete all containers

$ docker rm $(docker ps -a -q)

Delete all images

$ docker rmi $(docker images -q)

List dangling volumes

$ docker volume ls -f dangling=true

Remove dangling volumes:

$ docker volume rm $(docker volume ls -f dangling=true -q)

Check VM time compared to host time (for OSX drift issues)

$ docker run --rm -it busybox date -u +'%s'; date -u +'%s'

## Time

irb> Time.now.strftime('%Y-%m-%d-%I-%M-%S-%L')
2016-09-13-10-57-05-879

irb> Time.now.strftime('%Y-%m-%d-%I-%M-%S-%Z')
2016-09-13-10-57-05-EDT

irb> Time.now.utc.strftime('%Y-%m-%d-%I-%M-%S-%Z')
2016-09-13-10-57-05-UTC

shell function

    timestamp () {
      date +"%Y-%m-%d-%H-%M-%S-%Z" $1
    }

## Misc/Shell

Find utf files:
$ find . -type f -exec file --mime {} \+ | grep utf

Search man page summary for string
$ man -k <string>

See also:
$ apropos <string>

Recursive find-and-replace (Perl)

# TODO: test command to remove trailing whitespace in ruby files.

$ perl -pi -e 's/oldstring/newstring/g' `find ./ -name *.html`

## vim

- vim system clipboard register: \*

Execute specific version of installed gem command

$ gem*executable \_VERSION*

# e.g.

$ rails _5.0.0_ --version
$ rails _4.2.0_ --version

Note that often seems to not work, uncertain why.

Back after following a tag:

    ctrl-t

Delete lines matching:

:g/profile/d

Not matching:

:g!/profile/d

http://vim.wikia.com/wiki/Delete_all_lines_containing_a_pattern

## emacs

Emacs client/server, taken from https://news.ycombinator.com/item?id=9394144

# Run this somewhere (launchd?)

$ emacs -nw --daemon

# To edit file

$ emacsclient -nw filename

# To kill it

$ pkill emacs

# or

$ emacsclient -e '(kill-emacs)'

Reload projectile cache

projectile-invalidate-cache
SPC p I

## curl

POST
$ curl -X POST -d @filename.txt http://example.com/path/to/resource --header "Content-Type:application/json"

## git

Git short sha of HEAD

$ git rev-parse --short HEAD

Rebase branch onto branch that has had changes

$ git rebase --onto master HEAD~3

Show files currently being ignored

$ git ls-files --others -i --exclude-standard

Remove submodule (as of git 2 at least). This seems to do everything needed

$ git submodule deinit path/to/submodule
$ git rm -f path/to/submodule
$ rm -f .git/modules/path/to/submodule # not really required

## macOS

Fix occasionally broken dock icons:

sudo find /private/var/folders/ -name com.apple.iconservices -exec rm -rf {} \;
sudo rm -rf /Library/Caches/com.apple.iconservices.store
killall Dock

Flush DNS Cache (Yosemite)

# 10.9?

$ sudo discoveryutil udnsflushcaches

# 10.10

$ sudo dscacheutil -flushcache

Enable VNC from command line:

# (note: natpmpc -a 5900 5900 tcp 3600)

$ sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -access -off -restart -agent -privs -all -allowAccessFor -allUsers

# or

$ sudo defaults write /var/db/launchd.db/com.apple.launchd/overrides.plist com.apple.screensharing -dict Disabled -bool false
sudo launchctl load /System/Library/LaunchDaemons/com.apple.screensharing.plist

# see also: http://pivotallabs.com/enabling-os-x-screen-sharing-from-the-command-line/)

# see also: http://support.apple.com/kb/HT6175?viewlocale=en_US

## Networking

Open up a port externally

# Depends on router, is another command for the other thing...

$ natpmpc -a 9077 9000 tcp 36000

## Objective-C

Extern pattern:

.h: extern NSString *const WorkoutCellIdentifier;
.m: NSString *const WorkoutCellIdentifier = @"WorkoutCellIdentifier";

- SSH

  Proxy

  ssh -L [bind_address:]port:host:hostport]

  ssh -L 8080:foobar.com:80

  Proper way to update host key when it has changed:

  ssh-keygen -R hostname

## Ruby

Splats (from SO)

splats: when used as in LHS-like situation (any time you're invoking it on a
value), splat splits an array up into separate values, and when used in a
RHS-like situation (any time you're using it on some previously undefined
variables), it groups separate values into an array. So a,b,*c = d,e,f,*g
sets a=d, b=e, and c=[f,g0,g1,g2,...,gn], where g=[g0,g1,g2,...,gn]
