-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- use `vim.keymap.set` instead
local map = vim.keymap.set

map("n", "<leader><space>", ":noh<cr>")

map({ "n", "v" }, "<c-/>", "gc", { desc = "Testing", remap = true })
map({ "n", "v" }, "<leader>c", "gcc", { desc = "Testing", remap = true })
