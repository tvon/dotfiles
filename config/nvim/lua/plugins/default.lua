return {

  { "chriskempson/vim-tomorrow-theme" },
  { "reedes/vim-colors-pencil" },

  -- {
  --   "LazyVim/LazyVim",
  --   opts = {
  --     colorscheme = "Tomorrow-Night",
  --   },
  -- },

  -- { "hashivim/vim-terraform" },
  -- { "hashivim/vim-hashicorp-tools" },
}
