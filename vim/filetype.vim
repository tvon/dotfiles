if exists("did_load_filetypes")
  finish
endif

augroup vagrant
  au!
  au BufRead,BufNewFile Vagrantfile setlocal filetype=ruby
  au BufRead,BufNewFile Gemfile setlocal filetype=ruby
  au BufRead,BufNewFile Capfile setlocal filetype=ruby
augroup END

augroup jenkinsfile
  au BufReadPost Jenkinsfile setlocal syntax=groovy
  au BufReadPost Jenkinsfile setlocal filetype=groovy
augroup END

augroup golang
  au Filetype go setlocal listchars=tab:\ \ ,trail:· sw=4 ts=4
augroup END

au FileType markdown vmap <Leader><Bslash> :EasyAlign*<Bar><Enter>
