""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" general configuration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible
syntax enable
filetype plugin indent on
set autoindent
set backspace=indent,eol,start
set belloff=all
set complete=".,w,b,u,t"
set display=lastline
set formatoptions="tcqj"
set history=10000
set hlsearch "' is set by default
set incsearch "' is set by default
set nolangremap "' is enabled by default
set langremap "' is disabled by default
set laststatus=2 "' defaults to 2 (statusline is always shown)
set nrformats="bin,hex"
set ruler
set sessionoptions="blank,buffers,curdir,folds,help,tabpages,winsize"
set showcmd
set smarttab
set tabpagemax=50
set tags="./tags;,tags"
set ttyfast
set viminfo="!,'100,<50,s10,h"
set wildmenu

set clipboard=unnamed

" Default to bash syntax instead of sh syntax
let g:is_bash=1

filetype plugin indent on
syntax enable

set number
set laststatus=2

set smartcase " uppercase only matters if it is present in search term

set number
set cursorline " can cause performance problems

" Nicer listchars
set listchars=tab:»·,trail:·
set list

set display+=lastline
set laststatus=2
set modeline

" Sane defaults
set shiftwidth=2 tabstop=2 expandtab

" Persistent undo
set undofile
set undodir=$HOME/.vim/undo

set undolevels=1000
set undoreload=10000

" This just makes more sense to me
set splitbelow
set splitright

set foldenable
set foldmethod=syntax
set foldlevelstart=3

let mapleader=","

map ,, :A<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" misc configuration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

map <leader>d<space> :%s/ \+$//<cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" quickfix helpers
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Open quickfix window when Ggrep is run
autocmd QuickFixCmdPost *grep* cwindow

map <leader>qo :cwindow<cr>
map <leader>qc :ccl<cr>
map <leader>cn :cn<cr>
map <leader>cp :cp<cr>

map <leader>S :source ~/.vimrc<cr>
nnoremap <space> :noh<cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" gtags (https://www.gnu.org/software/global/manual/global.html#Vim-editor)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

map <C-n> :cn<CR>
"map <C-p> :cp<CR>
map <C-\>^] :GtagsCursor<CR>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Local configs
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Source `/.config/nvim/local.vim if it exists
let s:host_nvimrc = expand('~/.vim') . '/local.vim'
if filereadable(s:host_nvimrc)
  execute 'source '.fnameescape(s:host_nvimrc)
endif
