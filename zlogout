#!/usr/bin/env zsh
#
# zlogout : run by interactive login shells
#
# Executes commands at logout.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Print the message.
cat <<-EOF

Thank you. Come again!
  -- Dr. Apu Nahasapeemapetilon
EOF
