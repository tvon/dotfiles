#!/usr/bin/env zsh

# Ensure home bin overrides everything
export PATH="$HOME/bin:$PATH"
