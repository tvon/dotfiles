#!/usr/bin/env zsh

if [[ "$OSTYPE" != darwin* ]]; then
  return 1
fi

alias ls="ls -CFG"

export BROWSER='open'

if [ -d "/Applications/Visual Studio Code.app/" ]
then
  export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
fi

# NOTE: Experiemental
export PGDATA=~/.local/var/postgres
