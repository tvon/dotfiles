#!/usr/bin/env zsh

# Stupid homebrew and paths with implied meaning
if [ -f /usr/local/bin/brew ]; then
  eval $(/usr/local/bin/brew shellenv)
elif [ -f /opt/homebrew/bin/brew ]; then
  eval $(/opt/homebrew/bin/brew shellenv)
fi

alias update-brew="brew update && brew outdated"
