#!/usr/bin/env zsh

if [[ "$OSTYPE" != linux* ]]; then
  return 1
fi

alias ls='ls --color=auto --classify'
