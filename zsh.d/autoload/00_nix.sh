#!/usr/bin/env zsh

# On Darwin we probably don't want to use nix...
if [[ "$OSTYPE" == darwin* ]]; then
  if [ "$USE_NIX" != true ]; then
    return 1
  fi
fi

# Local user install needs to be setup before we check for nix-env
# TODO: Account for a shared `/home` mount used on nixos and non-nixos
if [ -f ~/.nix-profile/etc/profile.d/nix.sh ]; then
  source ~/.nix-profile/etc/profile.d/nix.sh
  manpath+=(~/.nix-profile/share/man)
fi

if ! command -v nix-env > /dev/null
then
  return 1;
fi

function nix-search(){
  nix-env -qa \* -P | fgrep -i "$1";
}

function activate() {
  if [ $# -eq 0 ];
  then
    if [ -f .nix ];
    then
      nix-shell .nix -A localEnv
    else
      echo '.nix file not found' 1>&2
    fi
  else
    nix-shell ~/dev/environments/$1.nix -A localEnv
  fi
}

function nix-install() {
  nix-env -i $*
}
