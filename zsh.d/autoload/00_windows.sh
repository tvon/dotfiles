#!/usr/bin/env zsh

# Check for running under WSL

if [ -f /proc/sys/kernel/osrelease ]
then
  if grep -q 'Microsoft' /proc/sys/kernel/osrelease
  then
    # Use Windows Docker daemon
    export DOCKER_HOST="tcp://0.0.0.0:2375"

    # WSL has a umask of 000 which is freakin odd.
    umask 0022

    # WSL adds windows paths to the Linux environment because I guess someone
    # would want that.  Until the the work in build 17713 is released (and
    # /etc/wsl.conf supports it), this is the hack:
    #
    # https://github.com/Microsoft/WSL/issues/1493
    PATH=$(/usr/bin/printenv PATH | /usr/bin/perl -ne 'print join(":", grep { !/\/(c|d)\// } split(/:/));')
  fi
fi

