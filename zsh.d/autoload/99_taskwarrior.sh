#!/usr/bin/env zsh

if command -v task >/dev/null 2>&1
then
  if [ $(tput cols) -gt 200 ]
  then
    if [ $(tput lines) -gt 40 ]
    then
      task list
    fi
  fi
fi
