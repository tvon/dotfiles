#!/usr/bin/env zsh

alias dcr="docker-compose run"
alias dcb="docker-compose build"
alias dcstart="docker-compose start"
alias dcstop="docker-compose stop"
