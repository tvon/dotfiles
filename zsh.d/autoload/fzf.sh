#!/usr/bin/env zsh

FZF_HOME=""

if [ -f "${HOME}"/.fzf/bin/fzf ]; then
  path+=($HOME/.fzf/bin)
  manpath+=($HOME/.fzf/man)

  FZF_HOME=$HOME/.fzf
fi

if [ -f "/opt/homebrew/bin/fzf" ]; then
  FZF_HOME=/opt/homebrew/opt/fzf
fi

if [ -d "${FZF_HOME}" ]; then
  source <(fzf --zsh)
  # source $FZF_HOME/shell/completion.zsh
  # source "${FZF_HOME}"/shell/key-bindings.zsh

  export FZF_THEME='--color=bg+:#3B4252,bg:#2E3440,spinner:#81A1C1,hl:#616E88,fg:#D8DEE9,header:#616E88,info:#81A1C1,pointer:#81A1C1,marker:#81A1C1,fg+:#D8DEE9,prompt:#81A1C1,hl+:#81A1C1'

  ## tmux popup at bottom of terminal
  # export FZF_OPTS="--height 20% --tmux bottom,20% --layout reverse --border top --layout=reverse --inline-info "

  ##tmux popup in center of terminal
  # export FZF_OPTS="--height 20% --tmux --layout reverse --border top --layout=reverse --inline-info "
  export FZF_OPTS="--layout=reverse --inline-info"

  export FZF_DEFAULT_OPTS="${FZF_OPTS} ${FZF_THEME}"

fi
