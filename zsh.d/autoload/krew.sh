#!/usr/bin/env zsh

if command -v kubectl-krew >/dev/null 2>&1
then
  export PATH="${PATH}:${HOME}/.krew/bin"
fi
