#!/usr/vin/env zsh

if [[ -a "/usr/local/bin/lesspipe.sh" ]];
then
  export LESSOPEN="|/usr/local/bin/lesspipe.sh %s" LESS_ADVANCED_PREPROCESSOR=1
fi
