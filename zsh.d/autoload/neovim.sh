#!/usr/bin/env zsh
#

if command -v nvim > /dev/null 2>&1
then
  export EDITOR=nvim
  export VISUAL=nvim
  export GIT_EDITOR=nvim

  alias vim='nvim'
  alias view='nvim -R'
  alias vimdiff='nvim -d'

  alias vim-light='export VIM_BACKGROUND=light VIM_COLORSCHEME=pencil'
fi
