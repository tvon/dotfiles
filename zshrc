# zshrc : run by interactive login shells
# zmodload zsh/zprof

# Start configuration added by Zim install {{{
#
# User configuration sourced by interactive shells
#

# -----------------
# Zsh configuration
# -----------------

#
# History
#

# Remove older command from the history if a duplicate is to be added.
setopt HIST_IGNORE_ALL_DUPS

#
# Input/output
#

# Set editor default keymap to emacs (`-e`) or vi (`-v`)
bindkey -e

# Prompt for spelling correction of commands.
#setopt CORRECT

# Customize spelling correction prompt.
#SPROMPT='zsh: correct %F{red}%R%f to %F{green}%r%f [nyae]? '

# Remove path separator from WORDCHARS.
WORDCHARS=${WORDCHARS//[\/]}


# --------------------
# Module configuration
# --------------------

#
# completion
#

# Set a custom path for the completion dump file.
# If none is provided, the default ${ZDOTDIR:-${HOME}}/.zcompdump is used.
#zstyle ':zim:completion' dumpfile "${ZDOTDIR:-${HOME}}/.zcompdump-${ZSH_VERSION}"

autoload -U +X bashcompinit && bashcompinit

#
# git
#

# Set a custom prefix for the generated aliases. The default prefix is 'G'.
#zstyle ':zim:git' aliases-prefix 'g'

#
# input
#

# Append `../` to your input for each `.` you type after an initial `..`
#zstyle ':zim:input' double-dot-expand yes

#
# termtitle
#

# Set a custom terminal title format using prompt expansion escape sequences.
# See http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html#Simple-Prompt-Escapes
# If none is provided, the default '%n@%m: %~' is used.
#zstyle ':zim:termtitle' format '%1~'

#
# zsh-autosuggestions
#

# Customize the style that the suggestions are shown with.
# See https://github.com/zsh-users/zsh-autosuggestions/blob/master/README.md#suggestion-highlight-style
#ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=10'
#ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#ff00ff,bg=cyan,bold,underline"
#ZSH_AUTOSUGGEST_STRATEGY


#
# zsh-syntax-highlighting
#

# Set what highlighters will be used.
# See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters.md
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

# Customize the main highlighter styles.
# See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters/main.md#how-to-tweak-it
#typeset -A ZSH_HIGHLIGHT_STYLES
#ZSH_HIGHLIGHT_STYLES[comment]='fg=10'

# ------------------
# Initialize modules
# ------------------

ZIM_CONFIG_FILE=~/.config/zsh/zimrc

# Install missing modules and update ${ZIM_HOME}/init.zsh if missing or outdated.
if [[ ! ${ZIM_HOME}/init.zsh -nt ${ZIM_CONFIG_FILE:-${ZDOTDIR:-${HOME}}/.zimrc} ]]; then
  source ${ZIM_HOME}/zimfw.zsh init -q
fi
source ${ZIM_HOME}/init.zsh
# source ${ZIM_HOME}/init.zsh

# ------------------------------
# Post-init module configuration
# ------------------------------

#
# zsh-history-substring-search
#

# Bind ^[[A/^[[B manually so up/down works both before and after zle-line-init
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

# Bind up and down keys
zmodload -F zsh/terminfo +p:terminfo
if [[ -n ${terminfo[kcuu1]} && -n ${terminfo[kcud1]} ]]; then
  bindkey ${terminfo[kcuu1]} history-substring-search-up
  bindkey ${terminfo[kcud1]} history-substring-search-down
fi

bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
# }}} End configuration added by Zim install

# Functions
fpath+=("$HOME/.zsh.d/functions/")
fpath+=("/opt/homebrew/share/zsh/site-functions")
fpath+=("/usr/local/share/zsh/site-functions")

autoload docker-clean-all
autoload docker-health
autoload docker-image-sha
autoload docker-labels
autoload docker-network-remove
autoload docker-time
autoload git-sha
autoload httpd-server
autoload securerandomhex
autoload timestamp
autoload tmux-sort-windows
autoload tmux-rename-window
autoload vimc
autoload notify-macos
autoload notify-ios
autoload notify-all
autoload watch-vmss
autoload venv

# From fiddling with truecolor terminal configs, might have a custom termcap.
alias ssh="TERM=xterm-256color ssh"

# Misc aliases
alias be="bundle exec"
alias brake="bundle exec rake"
alias brails="bundle exec rails"
alias info="info --vi-keys"
alias k="kubectl"
alias kk="kubectl config set-context --current --namespace "
#export do="--dry-run=client -o yaml"

if command -v hub >  /dev/null
then
  alias git=hub
fi

alias dockernotary="notary -s https://notary.docker.io -d ~/.docker/trust"

export NOTES="$HOME/Dropbox/notes"

function nn() {
  ts=$(timestamp)
  note="${NOTES}/${ts}.markdown"
  echo "Creating note: ${note}"
  vim ${NOTES}/$(timestamp).markdown
}

# export USE_NIX=true
export USE_HOMEBREW=true

# Not set in MacOS
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="${HOME}/.config"}

# if command -v asdf >  /dev/null
# then
#   export ASDF_DATA_DIR="${HOME}/.asdf"
#   path+=("$ASDF_DATA_DIR/shims")
# fi

if command -v mise >  /dev/null
then
  eval "$(mise activate zsh)"
fi

# setup direnv
eval "$(direnv hook zsh)"
# eval "$(asdf exec direnv hook zsh)"

# direnv() { asdf exec direnv "$@"; }


if command -v starship >  /dev/null
then
  eval "$(starship init zsh)"
fi

if [ -d ~/.zsh.d ]; then
  if [ -d ~/.zsh.d/autoload ]; then
    for conf in $(find ~/.zsh.d/autoload -name '*.sh'); do
      source "$conf"
    done
  fi
fi

if [ -f ~/.zshrc-local ]; then
  source ~/.zshrc-local
fi

#zprof >! $HOME/zprof.log
